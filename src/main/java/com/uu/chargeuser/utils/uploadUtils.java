package com.uu.chargeuser.utils;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class uploadUtils {

    /**
     * @param file     文件
     * @param path     文件存放路径
     * @param fileName 保存的文件名
     * @return
     */
    public static boolean upload(MultipartFile file, String path, String fileName) {

        //确定上传的文件名
        String realPath = path + "/" + fileName;

        File dest = new File(realPath);

        //判断文件父目录是否存在
        /*if (!dest.getParentFile().exists()) {
            dest.getParentFile().mkdir();
        }*/

        if  (!dest .exists()  && !dest .isDirectory()) {
            dest .mkdirs();
        }

        try {
            //保存文件
            file.transferTo(dest);
            return true;
        } catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }

    }

    public Map uploadImage(MultipartFile file){
        Map returnMap = new HashMap();
        returnMap.put("code","0");
        //判断图片大小
        if (file.getSize() / 1024 / 1024 > 10){
            returnMap.put("msg","图片大小不能超过10M");
        }
        //判断上传文件格式
        String fileType = file.getContentType();
        if (fileType.equals("image/jpeg") || fileType.equals("image/png") || fileType.equals("image/gif")) {
            Date d = new Date();
            //System.out.println(d);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

            String dateStr = sdf.format(d);
            String webappPath = request.getSession().getServletContext().getRealPath("/");
            String relativePath = "uploads/image/" + dateStr;
            //System.out.println(relativePath);

            String path = webappPath + relativePath;
            //上传后保存的文件名(需要防止图片重名导致的文件覆盖)
            //获取文件名
            String fileName = file.getOriginalFilename();
            //获取文件后缀名
            String suffixName = fileName.substring(fileName.lastIndexOf("."));
            //重新生成文件名
            fileName = UUID.randomUUID()+suffixName;
            //System.out.println(path);
            if (this.upload(file, path, fileName)) {
                //文件存放的相对路径(一般存放在数据库用于img标签的src)
                relativePath = relativePath + "/" + fileName;
                returnMap.put("relativePath",relativePath);//前端根据是否存在该字段来判断上传是否成功
                returnMap.put("code","200");
                returnMap.put("msg","图片上传成功");
            }
            else{
                returnMap.put("msg","图片上传失败");
            }
        }else {
            returnMap.put("msg","图片格式不正确");
        }

        return returnMap;
    }


}
