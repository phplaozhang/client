package com.uu.chargeuser.utils;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class HttpUtil {

    public static String get(String url, HashMap<String, String> header) {
        String result = "";

        OkHttpClient client = new OkHttpClient();


        Request.Builder requestBuilder = new Request.Builder();
        requestBuilder.url(url);

        if(header.size() > 0){
            for(Map.Entry<String, String> entry : header.entrySet()){
                requestBuilder.addHeader(entry.getKey(), entry.getValue());
            }
        }

        requestBuilder.addHeader("Content-Type", "text/plain; charset=utf-8");
        requestBuilder.addHeader("cache-control", "no-cache");

        requestBuilder.get();
        Request request = requestBuilder.build();


        try {
            Response response = client.newCall(request).execute();
            if(response.isSuccessful()){
                return response.body().string();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        return result;
    }

    public static String post(String url, HashMap<String, String> header) {
        String result = "";

        OkHttpClient client = new OkHttpClient();


        Request.Builder requestBuilder = new Request.Builder();
        requestBuilder.url(url);

        if(header.size() > 0){
            for(Map.Entry<String, String> entry : header.entrySet()){
                requestBuilder.addHeader(entry.getKey(), entry.getValue());
            }
        }
        requestBuilder.addHeader("Content-Type", "text/plain; charset=utf-8");
        requestBuilder.addHeader("cache-control", "no-cache");

        requestBuilder.post(null);
        Request request = requestBuilder.build();


        try {
            Response response = client.newCall(request).execute();
            if(response.isSuccessful()){
                return response.body().string();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        return result;
    }


}
