package com.uu.chargeuser.exception;

/**
 * 自定义业务异常
 */
public class CmdcException extends RuntimeException {
    private int code;
    private String message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CmdcException(ErrorEnum errorEnum) {
        this.code = errorEnum.getCode();
        this.message = errorEnum.getMsg();
    }

    public CmdcException(int code, String message) {
        this.code = code;
        this.message = message;
    }
}