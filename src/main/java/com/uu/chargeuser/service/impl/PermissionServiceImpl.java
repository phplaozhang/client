package com.uu.chargeuser.service.impl;

import com.uu.chargeuser.entity.Permission;
import com.uu.chargeuser.repository.PermissionRepository;
import com.uu.chargeuser.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
/**
 * (permissionService)表服务实现类
 *
 * @author cmt
 */
@Service("permissionService")
public class PermissionServiceImpl implements PermissionService {
    @Autowired
    PermissionRepository permissionRepository;
    @Override
    public List<Permission> findAll() {
        return permissionRepository.findAll();
    }
}
