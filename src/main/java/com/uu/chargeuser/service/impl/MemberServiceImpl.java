package com.uu.chargeuser.service.impl;

import com.uu.chargeuser.entity.Member;
import com.uu.chargeuser.dao.MemberDao;
import com.uu.chargeuser.service.MemberService;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import javax.annotation.Resource;
import java.util.List;

import org.springframework.data.domain.Page;

/**
 * (Member)表服务实现类
 *
 * @author cmt
 */
@Service("memberService")
public class MemberServiceImpl implements MemberService {
    @Resource
    private MemberDao memberDao;

    @Override
    public Member findBySn(String sn) {
        return memberDao.findBySn(sn);
    }

    @Override
    public Member queryById(String sn) {

        return this.memberDao.findBySn(sn);
    }

    @Override
    public List<Member> getall() {
        return this.memberDao.findAll();
    }

    @Override
    public Page<Member> queryAllByLimit(int offset, int limit) {
        return this.memberDao.findAll(PageRequest.of((offset - 1)
                * limit, limit));
    }

    @Override
    public Member insert(Member member) {

        return this.memberDao.save(member);
    }


    @Override
    public Member update(Member member) {

        return this.memberDao.save(member);
    }

    @Override
    public boolean deleteById(String sn) {
        return false;
    }
}


