package com.uu.chargeuser.service;

import com.uu.chargeuser.entity.dto.MemberDTO;

public interface MemberDTOService {
    MemberDTO findBySn(String sn);
}
