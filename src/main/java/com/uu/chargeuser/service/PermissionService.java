package com.uu.chargeuser.service;

import com.uu.chargeuser.entity.Permission;

import java.util.List;
/**
 * (Permission)表服务接口
 *
 * @author cmt
 */
public interface PermissionService {
    //查询所有列表
    List<Permission> findAll();
}
