package com.uu.chargeuser.service;

import com.uu.chargeuser.entity.Member;

import java.util.List;

import org.springframework.data.domain.Page;

/**
 * (Member)表服务接口
 *
 * @author cmt
 */
public interface MemberService {
    Member findBySn(String sn);

    Member queryById(String sn);

    Page<Member> queryAllByLimit(int offset, int limit);

    Member insert(Member member);

    Member update(Member member);

    boolean deleteById(String sn);

    List<Member> getall();
}

