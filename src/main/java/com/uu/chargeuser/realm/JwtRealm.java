package com.uu.chargeuser.realm;

import com.uu.chargeuser.common.Constant;
import com.uu.chargeuser.entity.Member;
import com.uu.chargeuser.entity.Permission;
import com.uu.chargeuser.entity.Role;
import com.uu.chargeuser.service.impl.MemberServiceImpl;
import com.uu.chargeuser.shirotoken.JwtToken;
import com.uu.chargeuser.utils.JwtUtil;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
/**
 * Shiro自定义JwtRealm授权认证器
 * @author cmt
 */
@Component
public class JwtRealm extends AuthorizingRealm {
    @Resource
    private MemberServiceImpl memberServiceImpl;
    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof JwtToken;
    }

    @Override
    /**
     * 授权 获取用户的角色和权限
     *@param  [principals]
     *@return org.apache.shiro.authz.AuthorizationInfo
     */
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        //1. 从 PrincipalCollection 中来获取登录用户的信息
        String token=principals.toString();
        String sn = JwtUtil.getUserId(token);
        Member member = memberServiceImpl.findBySn(sn);
        //2.添加角色和权限
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
        for (Role role : member.getRoles()) {
            //2.1添加角色
            simpleAuthorizationInfo.addRole(role.getRoleName());
            for (Permission permission : role.getPermissions()) {
                //2.1.1添加权限
                simpleAuthorizationInfo.addStringPermission(permission.getPermissionUrl());
            }
        }
        return simpleAuthorizationInfo;
    }

    @Override
    /**
     * 认证 判断token的有效性
     *@param  [token]
     *@return org.apache.shiro.authc.AuthenticationInfo
     */
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken auth) throws AuthenticationException {
        String token = (String) auth.getCredentials();
        // 解密获得username，用于和数据库进行对比
        String sn = JwtUtil.getUserId(token);
        if (sn == null) {
            throw new IncorrectCredentialsException("token无效，请重新登录");
        }
        //数据库比对
        Member member = memberServiceImpl.queryById(sn);
        if (member == null) {
            throw new UnknownAccountException("用户不存在!");
        }
        //操作时校验的是非对称加密是否成立.
        if (!JwtUtil.verify(token, Constant.TOKEN_SECRET)) {
            throw new AuthenticationException("账号或密码错误");
        }
        //5. 根据用户的情况, 来构建 AuthenticationInfo 对象并返回. 通常使用的实现类为: SimpleAuthenticationInfo
        SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(token, token, this.getName());
        return info;
    }
}
