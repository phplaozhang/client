package com.uu.chargeuser.controller;

import com.github.wxpay.sdk.WXPayConfig;
import com.github.wxpay.sdk.WXPayUtil;
import com.uu.chargeuser.extend.Db;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("wechatNotify")
public class WechatNotifyController {

    @PostMapping("payNotify")
    public String payNotify(HttpServletRequest request){
        InputStream is = null;
        String xmlBack = "<xml><return_code><![CDATA[FAIL]]></return_code><return_msg><![CDATA[报文为空]]></return_msg></xml> ";
        try {
            is = request.getInputStream();
            // 将InputStream转换成String
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }

            String notifyStr = sb.toString();
            Map<String, String> resultMap = WXPayUtil.xmlToMap(notifyStr);
            String appid = resultMap.get("appid");

            List where  = new ArrayList(){{
                add(new ArrayList<Object>(){{
                    add("wechat_appid");
                    add("=");
                    add(appid);
                }});
            }};

            Db db = new Db();
            Map platform = db.table("platform").where(where).find("*");


            String wechat_appid = platform.get("wechat_appid").toString();
            String wechat_mchid = platform.get("wechat_mchid").toString();
            String wechat_key = platform.get("wechat_key").toString();
            String wechat_appsecret = platform.get("wechat_appsecret").toString();
            //微信支付配置
            WXPayConfig wxPayConfig = new WXPayConfig() {
                @Override
                public String getAppID() {
                    return wechat_appid;
                }

                @Override
                public String getMchID() {
                    return wechat_mchid;
                }

                @Override
                public String getKey() {
                    return wechat_key;
                }

                @Override
                public InputStream getCertStream() {
                    return null;
                }

                @Override
                public int getHttpConnectTimeoutMs() {
                    return 0;
                }

                @Override
                public int getHttpReadTimeoutMs() {
                    return 0;
                }
            };
            xmlBack = com.uu.chargeuser.extend.Wechat.payNotify(wxPayConfig, notifyStr);
        } catch (Exception e) {
            System.out.println("微信手机支付回调通知失败：" + e);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return xmlBack;

    }
}
