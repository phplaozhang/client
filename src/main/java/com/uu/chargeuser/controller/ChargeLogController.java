package com.uu.chargeuser.controller;

import com.alibaba.fastjson.JSONObject;
import com.uu.chargeuser.extend.Db;
import com.uu.chargeuser.result.Result;
import com.uu.chargeuser.result.ResultFactory;
import com.uu.chargeuser.utils.JwtUtil;
import com.uu.chargeuser.utils.dataUtil;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * (ChargeLog)表控制层
 *
 * @author makejava
 * @since 2021-03-08 09:03:20
 */
@RestController
@RequestMapping("chargeLog")
public class ChargeLogController {

    @GetMapping("list")
    public Result list(HttpServletRequest request,int limit, int page){
        String memberSn = JwtUtil.getUserId(request.getHeader("token"));

        int offset = (page - 1) * limit;

        Db db = new Db();
        String list_sql = "SELECT charge_log.id,charge_log.start_time,charge_log.end_time,charge_log.socket_number,charge_log.station_sn,charge_log.money_total,station.name as station_name from charge_log left join station on charge_log.station_sn = station.sn where  charge_log.member_sn = ? and charge_log.end_time > 0 order by charge_log.start_time desc OFFSET ? ROWS FETCH NEXT ?  ROWS ONLY";
        List parameter = new ArrayList<Object>(){{
            add(memberSn);
            add(offset);
            add(limit);
        }};
        List<Map> list = db.executeQueryBySql(list_sql,parameter);
        //System.out.println(db.getLastSql());

        String count_sql = "SELECT count(*) as count from charge_log  where member_sn = ? and charge_log.end_time > 0";
        List parameter1 = new ArrayList<Object>(){{
            add(memberSn);
        }};
        List<Map> count_list = db.executeQueryBySql(count_sql,parameter1);
        int count = (int) count_list.get(0).get("count");

        for(int i = 0; i< list.size(); i ++){
            Map charge_log = list.get(i);

            charge_log.put("money_total",dataUtil.formatMoney(charge_log.get("money_total"),2));

            long charging_seconds = ((long)charge_log.get("end_time") - (long)charge_log.get("start_time")) / 1000;
            int hour = (int)Math.floor(charging_seconds / 3600);
            int minute = (int)Math.floor((charging_seconds - (hour * 3600))  / 60);
            int second = (int)Math.floor((charging_seconds - (hour * 3600) - (minute * 60)) % 60);
            charge_log.put("_charge_hour",hour);
            charge_log.put("_charge_minute",minute);
            charge_log.put("_charge_second",second);

            String create_time = dataUtil.stampToDate((long) charge_log.get("start_time"));
            charge_log.put("_start_time",create_time);

            String end_time = dataUtil.stampToDate((long) charge_log.get("end_time"));
            charge_log.put("_end_time",end_time);

            list.set(i,charge_log);
        }
        JSONObject data = new JSONObject();
        data.put("count",count);
        data.put("list",list);
        return ResultFactory.buildSuccessResult(data);
    }

    //充电记录详情
    @GetMapping("detail")
    public Result detail(int id){
        Db db = new Db();

        List parameter = new ArrayList<Object>(){{
            add(id);
        }};
        String sql = "select charge_log.state,charge_log.member_type,charge_log.member_sn,charge_log.start_time,charge_log.end_time,charge_log.money_total,charge_log.member_balance,charge_log.station_sn,charge_log.socket_number,charge_log.end_reason,member.avatar,member.nickname,station.name as station_name,project.name as project_name from charge_log left join member on member.sn = charge_log.member_sn left join station on station.sn = charge_log.station_sn left join project on project.sn = charge_log.project_sn where charge_log.id = ?";
        List list = db.executeQueryBySql(sql, parameter);
        Map charge_log = (Map) list.get(0);

        charge_log.put("money_total",dataUtil.formatMoney(charge_log.get("money_total"),2));
        charge_log.put("member_balance",dataUtil.formatMoney(charge_log.get("member_balance"),2));

        long charging_seconds = ((long)charge_log.get("end_time") - (long)charge_log.get("start_time")) / 1000;
        int hour = (int)Math.floor(charging_seconds / 3600);
        int minute = (int)Math.floor((charging_seconds - (hour * 3600))  / 60);
        int second = (int)Math.floor((charging_seconds - (hour * 3600) - (minute * 60)) % 60);
        charge_log.put("_charge_hour",hour);
        charge_log.put("_charge_minute",minute);
        charge_log.put("_charge_second",second);

        String create_time = dataUtil.stampToDate((long) charge_log.get("start_time"));
        charge_log.put("_start_time",create_time);

        String end_time = dataUtil.stampToDate((long) charge_log.get("end_time"));
        charge_log.put("_end_time",end_time);

        return ResultFactory.buildSuccessResult(charge_log);
    }


}
