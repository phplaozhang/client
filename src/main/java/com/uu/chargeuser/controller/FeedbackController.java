package com.uu.chargeuser.controller;

import com.uu.chargeuser.extend.Db;
import com.uu.chargeuser.result.Result;
import com.uu.chargeuser.result.ResultFactory;
import com.uu.chargeuser.utils.JwtUtil;
import com.uu.chargeuser.utils.dataUtil;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * (Feedback)表控制层
 *
 * @author makejava
 * @since 2021-03-09 11:33:01
 */
@RestController
@RequestMapping("feedback")
public class FeedbackController {

    //提交留言
    @PostMapping("submit")
    public Result repairSubmit(HttpServletRequest request, String type, String name, String mobile, String content){
        String memberSn = JwtUtil.getUserId(request.getHeader("token"));

        if (type == null || (!type.equals("1") && !type.equals("2"))) {
            return ResultFactory.buildFailResult("留言类型有误");
        }

        if(type.equals("2") && (name == null || name.equals(""))){
            return ResultFactory.buildFailResult("姓名不能为空");
        }

        if(name == null || name.equals("")){
            name = "";
        }

        if (mobile == null || mobile.equals("")) {
            return ResultFactory.buildFailResult("手机号码不能为空");
        }

        if(!dataUtil.isMobile(mobile)){
            return ResultFactory.buildFailResult("手机号码不正确");
        }

        if (content == null || content.equals("")) {
            return ResultFactory.buildFailResult("内容不能为空");
        }

        Db db = new Db();
        long create_time = System.currentTimeMillis();

        Map insertData = new HashMap(){
            {
                put("type", type);
                put("member_sn", memberSn);
                put("mobile", mobile);
                put("content", content);
                put("create_time", create_time);
                put("update_time", create_time);
            }
        };
        insertData.put("name", name);

        //System.out.println(insertData);
        int res = db.table("feedback").insert(insertData);
        //System.out.println(db.getLastSql());
        if (res > 0) {
            return ResultFactory.buildSuccessResult(insertData);
        }else{
            return ResultFactory.buildFailResult("失败");
        }

    }

}
