package com.uu.chargeuser.controller;

import com.alibaba.fastjson.JSONObject;
import com.uu.chargeuser.extend.Db;
import com.uu.chargeuser.result.Result;
import com.uu.chargeuser.result.ResultFactory;
import com.uu.chargeuser.utils.JwtUtil;
import com.uu.chargeuser.utils.dataUtil;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * (MonthCard)表控制层
 *
 * @author makejava
 * @since 2021-03-10 16:04:05
 */
@RestController
@RequestMapping("monthCard")
public class MonthCardController {

    @GetMapping("detail")
    public Result detail(@RequestHeader("token")String token){
        String memberSn = JwtUtil.getUserId(token);
        int is_month_card = 0;
        Map month_card = new HashMap();
        List month_card_settings = new ArrayList();
        int  month_card_max_power = - 1;
        Db db = new Db();

        Map member = db.table("member").where(new ArrayList(){{
            add(new ArrayList<Object>(){{
                add("sn");
                add("=");
                add(memberSn);
            }});
        }}).find("project_sn");

        month_card = db.table("month_card left join project on project.sn = month_card.project_sn").where(new ArrayList() {{
            add(new ArrayList() {{
                add("member_sn");
                add("=");
                add(memberSn);
            }});
        }}).order("create_time desc").find("month_card.*,project.name as project_name,project.month_card_max_power");

        if(month_card.size() == 0 && (int) member.get("project_sn") > 0){
            Map proect = db.table("project").where(new ArrayList() {{
                add(new ArrayList() {{
                    add("sn");
                    add("=");
                    add(member.get("project_sn"));
                }});
            }}).find("month_card_max_power");
            month_card_max_power = (int) proect.get("month_card_max_power");
        }

        if(month_card.size() > 0){
            int month_card_remain_second = (int) month_card.get("remain_second");
            int month_card_hour = (int)Math.floor(month_card_remain_second / 3600);
            int month_card_minute = (int)Math.floor((month_card_remain_second - (month_card_hour * 3600))  / 60);
            int month_card_second = (int)Math.floor((month_card_remain_second - (month_card_hour * 3600) - (month_card_minute * 60)) % 60);
            month_card.put("_remain_hour",month_card_hour);
            month_card.put("_remain_minute",month_card_minute);
            month_card.put("_remain_second",month_card_second);
            String _end_time = dataUtil.stampToDate((long) month_card.get("end_time"));
            month_card.put("_end_time",_end_time);
            month_card.put("__end_time",_end_time.substring(0,10));

            month_card_max_power = (int) month_card.get("month_card_max_power");
        }

        month_card_settings = db.table("month_card_setting").where(new ArrayList(){{
            add(new ArrayList(){{
                add("project_sn");
                add("=");
                add(member.get("project_sn"));
            }});
        }}).order("days asc").select("*");

        for (int i = 0; i < month_card_settings.size(); i++) {
            Map month_card_setting = (Map) month_card_settings.get(i);
            month_card_setting.put("price", dataUtil.formatMoney(month_card_setting.get("price"),2));
            month_card_setting.put("old_price", dataUtil.formatMoney(month_card_setting.get("old_price"),2));
            int remain_second = (int) month_card_setting.get("remain_second");
            float hour = Math.round(remain_second / 3600 * 100) / 100;
            month_card_setting.put("_remain_hour",hour);

            month_card_settings.set(i,month_card_setting);
        }


        if(month_card.size() > 0 || month_card_settings.size() > 0){
            is_month_card = 1;
        }

        int has_month_card = 0;
        if(month_card.size() > 0){
            has_month_card = 1;
        }

        JSONObject data = new JSONObject();
        data.put("month_card_setting",month_card_settings);
        data.put("month_card",month_card);
        data.put("has_month_card",has_month_card);
        data.put("is_month_card",is_month_card);
        data.put("month_card_max_power",month_card_max_power);
        return ResultFactory.buildSuccessResult(data);
    }

}
