package com.uu.chargeuser.controller;

import com.uu.chargeuser.extend.Db;
import com.uu.chargeuser.result.Result;
import com.uu.chargeuser.result.ResultFactory;
import com.uu.chargeuser.utils.JwtUtil;
import net.sf.json.JSONArray;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Map;

/**
 * (Platform)表控制层
 *
 * @author makejava
 * @since 2021-03-09 14:04:56
 */
@RestController
@RequestMapping("platform")
public class PlatformController {

    @GetMapping("detail")
    public Result detail(HttpServletRequest request){
        String memberSn = JwtUtil.getUserId(request.getHeader("token"));

        Db db = new Db();

        Map member = db.table("member").where(new ArrayList(){{
            add(new ArrayList(){{
                add("sn");
                add("=");
                add(memberSn);
            }});
        }}).find("platform_sn");

        String platformSn = (String) member.get("platform_sn");

        Map platform = db.table("platform").where(new ArrayList(){{
            add(new ArrayList(){{
                add("sn");
                add("=");
                add(platformSn);
            }});
        }}).find("sn,company_name,platform_name,telephone,is_subscribe,recharge_config,skins,station_repair_options,copyright,version,user_agreement,disclaimer,one_charge_minuts_options");


        JSONArray rechargeConfigArr = JSONArray.fromObject(platform.get("recharge_config"));
        platform.put("recharge_config_arr",rechargeConfigArr);

        String[] repairOptionsArr = platform.get("station_repair_options").toString().split("\\\n");
        platform.put("station_repair_options_arr",repairOptionsArr);

        String[] one_charge_minuts_options_arr = platform.get("one_charge_minuts_options").toString().split(",");
        platform.put("one_charge_minuts_options_arr",one_charge_minuts_options_arr);

        String[] one_charge_hours_options_arr = new String[one_charge_minuts_options_arr.length];

        for (int i = 0; i < one_charge_minuts_options_arr.length; i++) {
            int minut =  Integer.parseInt(one_charge_minuts_options_arr[i]) / 60;
            one_charge_hours_options_arr[i] = minut + "";
        }

        platform.put("one_charge_hours_options_arr",one_charge_hours_options_arr);

        return ResultFactory.buildSuccessResult(platform);

    }

}
