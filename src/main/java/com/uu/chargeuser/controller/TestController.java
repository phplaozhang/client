package com.uu.chargeuser.controller;

import com.uu.chargeuser.entity.Permission;
import com.uu.chargeuser.result.Result;
import com.uu.chargeuser.result.ResultFactory;
import com.uu.chargeuser.service.impl.MemberServiceImpl;
import com.uu.chargeuser.service.impl.PermissionServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping
@Validated
public class TestController {
    @Autowired
    MemberServiceImpl memberServiceImpl;
    @Resource
    PermissionServiceImpl permissionServiceImpl;
    //获取项目端口号
    @Value("${server.port}")
    private static String servePrort;

    @PostMapping(value = "/system",name = "测试主页")
    public Result system(String username, String password) {
        List<Permission> permissionList = permissionServiceImpl.findAll();
        System.out.println("permissionList:"+permissionList.toArray());
        return ResultFactory.buildSuccessResult("系统设置");
    }
    @PostMapping(value = "/membersPage",name = "用户管理")
    public Result membersPage() {
        return ResultFactory.buildSuccessResult("用户管理");
    }
    @PostMapping(value = "/rolesPage",name = "角色管理")
    public Result rolesPage() {
        return ResultFactory.buildSuccessResult("角色管理");
    }
    @PostMapping(value = "/permissionPage",name = "权限管理")
    public Result permissionPage() {
        return ResultFactory.buildSuccessResult("权限管理");
    }
    @PostMapping(value = "/roles/add",name = "权限管理添加")
    public Result rolesAdd() {
        return ResultFactory.buildSuccessResult("权限管理添加");
    }

}
