package com.uu.chargeuser.controller;

import com.alibaba.fastjson.JSONObject;
import com.uu.chargeuser.extend.Db;
import com.uu.chargeuser.result.Result;
import com.uu.chargeuser.result.ResultFactory;
import com.uu.chargeuser.utils.JedisUtil;
import com.uu.chargeuser.utils.JwtUtil;
import com.uu.chargeuser.utils.dataUtil;
import org.springframework.web.bind.annotation.*;
import redis.clients.jedis.Jedis;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * (Socket)表控制层
 *
 * @author makejava
 * @since 2021-03-06 13:37:49
 */
@RestController
@RequestMapping("socket")
public class SocketController{

    //正在充电数量
    @GetMapping("chargingCount")
    public Result chargingCount(HttpServletRequest request){
        String memberSn = JwtUtil.getUserId(request.getHeader("token"));
        Db db = new Db();
        String count_sql = "SELECT count(*) as count from socket  where socket.state = 2 and member_sn = ?";
        List parameter = new ArrayList<Object>(){{
            add(memberSn);
        }};
        List<Map> count_list = db.executeQueryBySql(count_sql,parameter);
        int count = (int) count_list.get(0).get("count");
        JSONObject data = new JSONObject();
        data.put("count",count);
        return ResultFactory.buildSuccessResult(data);
    }

    //正在充电列表
    @GetMapping("chargingList")
    public Result chargingList(HttpServletRequest request){
        String memberSn = JwtUtil.getUserId(request.getHeader("token"));

        Db db = new Db();
        String list_sql = "SELECT socket.sn,socket_number,station_sn,power_value,money_total,socket.start_time,station.name as station_name from socket left join station on socket.station_sn = station.sn where socket.state = 2 and socket.member_sn = ? order by socket.start_time desc OFFSET 0 ROWS FETCH NEXT 100  ROWS ONLY";
        List parameter = new ArrayList<Object>(){{
            add(memberSn);
        }};
        List<Map> list = db.executeQueryBySql(list_sql,parameter);

        String count_sql = "SELECT count(*) as count from socket  where socket.state = 2 and member_sn = ?";
        List<Map> count_list = db.executeQueryBySql(count_sql,parameter);
        int count = (int) count_list.get(0).get("count");

        for(int i = 0; i< list.size(); i ++){
            Map socket = list.get(i);
            socket.put("money_total", dataUtil.formatMoney(socket.get("money_total"),2));
            long charging_seconds = (System.currentTimeMillis() - (long)socket.get("start_time")) / 1000;
            int hour = (int)Math.floor(charging_seconds / 3600);
            int minute = (int)Math.floor((charging_seconds - (hour * 3600))  / 60);
            int second = (int)Math.floor((charging_seconds - (hour * 3600) - (minute * 60)) % 60);
            socket.put("charge_time",hour + ":" + minute + ":" + second);
            socket.put("_charge_hour",hour);
            socket.put("_charge_minute",minute);
            socket.put("_charge_second",second);
            list.set(i,socket);
        }
        JSONObject data = new JSONObject();
        data.put("count",count);
        data.put("list",list);
        return ResultFactory.buildSuccessResult(data);

    }

    //插座详情
    @GetMapping("detail")
    public Result detail(String sn){

        if(sn == null || sn.equals("")){
            return ResultFactory.buildFailResult("插座信息有误");
        }

        Db db = new Db();

        List parameter = new ArrayList<Object>(){{
            add(sn);
        }};
        String sql = "select project.charging_mode,project.fixed_money_options,project.float_charge_power,project.float_charge_minuts,project.money_degree,project.is_safe_charge,project.is_free,project.month_card_max_power,project.is_alipay,project.is_wechat,project.is_card,socket.state,socket.sn,socket_number,socket.station_sn,station.name as station_name,station.project_sn as project_sn,station.is_online from socket left join station on station.sn = socket.station_sn left join project on project.sn = station.project_sn where socket.sn = ?";
        List list = db.executeQueryBySql(sql, parameter);
        Map socket = (Map) list.get(0);

        socket.put("fixed_money_options",socket.get("fixed_money_options").toString().split(","));

        return ResultFactory.buildSuccessResult(socket);
    }

    //开始充电
    @PostMapping("start")
    public Result start(@RequestHeader("token")String token, String sn, String is_safe_charge, String fix_money){
        String memberSn = JwtUtil.getUserId(token);
        if(sn == null || sn.equals("")){
            return ResultFactory.buildFailResult("插座信息有误");
        }

        if(is_safe_charge == null || is_safe_charge.equals("")){
            is_safe_charge = "0";
        }
        if(fix_money == null || fix_money.equals("")){
            fix_money = "0";
        }

        Db db = new Db();

        Map member = db.table("member").where(new ArrayList(){{
            add(new ArrayList(){{
                add("sn");
                add("=");
                add(memberSn);
            }});
        }}).find("sn,type,one_charge_minuts");

        if(member.size() == 0){
            return ResultFactory.buildFailResult("无权操作");
        }

        String sql = "select project.charging_mode,project.money_degree,project.is_safe_charge,project.is_free,project.month_card_max_power,socket.sn,socket_number,socket.station_sn,station.project_sn as project_sn,station.is_online,platform.safe_money,platform.is_safe_charge as platform_is_safe_charge from socket left join station on station.sn = socket.station_sn left join project on project.sn = station.project_sn left join platform on platform.sn = station.platform_sn where socket.sn = ?";
        List parameter = new ArrayList<Object>(){{
            add(sn);
        }};
        List list = db.executeQueryBySql(sql, parameter);
        Map socket = (Map) list.get(0);

        if(socket.get("is_safe_charge").equals("0") || socket.get("platform_is_safe_charge").equals("0")){
            is_safe_charge = "0";
        }


        int remain_second = (int) member.get("one_charge_minuts") * 60;
        long create_time = System.currentTimeMillis();

        String redisKey = sn;
        HashMap redisMap = new HashMap() {{
            put("sn", sn + "");
            put("station_sn", socket.get("station_sn") + "");
            put("socket_number", socket.get("socket_number") + "");
            put("member_sn",memberSn + "");
            put("member_type",member.get("type") + "");
            put("safe_money",socket.get("safe_money") + "");
            put("charging_mode",socket.get("charging_mode") + "");
            put("remain_second",remain_second + "");
            put("cmd",1 + "");
            put("cmd_time",create_time + "");
        }};
        redisMap.put("is_safe_charge",is_safe_charge);
        redisMap.put("fix_money",fix_money);
        Jedis jedis = JedisUtil.getJedis();
        //查询缓存中是否存在
        if(jedis.exists(redisKey)){
            return ResultFactory.buildFailResult("此插座正在使用");
        }
        String hmset = jedis.hmset(redisKey, redisMap);
        if(hmset.equals("OK")){
            JSONObject cmdJson = new JSONObject();
            cmdJson.put("sn", sn + "");
            cmdJson.put("cmd" , "1");
            cmdJson.put("cmd_time",create_time + "");
            jedis.lpush("socket_cmd",cmdJson.toString());
        }
        JedisUtil.close(jedis);


        return ResultFactory.buildSuccessResult(new JSONObject());
    }

    //结束充电
    @PostMapping("end")
    public Result end(@RequestHeader("token")String token, String sn){
        String memberSn = JwtUtil.getUserId(token);

        long create_time = System.currentTimeMillis();
        String redisKey = sn;

        Jedis jedis = JedisUtil.getJedis();
        if(!jedis.exists(redisKey)){
            return ResultFactory.buildFailResult("此插座未开启，不可停止");
        }
        List<String> member_sn = jedis.hmget(redisKey, "member_sn");

        if(!member_sn.equals(memberSn)){
            return ResultFactory.buildFailResult("无权操作");
        }

        JSONObject cmdJson = new JSONObject();
        cmdJson.put("sn", sn + "");
        cmdJson.put("cmd" , "-1");
        cmdJson.put("cmd_time",create_time + "");
        jedis.lpush("socket_cmd",cmdJson.toString());
        JedisUtil.close(jedis);

        return ResultFactory.buildSuccessResult(new JSONObject());
    }



}
