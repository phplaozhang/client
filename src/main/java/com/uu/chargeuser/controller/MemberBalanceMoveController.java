package com.uu.chargeuser.controller;

import com.alibaba.fastjson.JSONObject;
import com.uu.chargeuser.extend.Db;
import com.uu.chargeuser.result.Result;
import com.uu.chargeuser.result.ResultFactory;
import com.uu.chargeuser.utils.JwtUtil;
import com.uu.chargeuser.utils.dataUtil;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * (MemberBalanceMove)表控制层
 *
 * @author makejava
 * @since 2021-03-10 19:06:27
 */
@RestController
@RequestMapping("memberBalanceMove")
public class MemberBalanceMoveController {

    //余额转让
    @PostMapping("balanceMove")
    public Result balanceMove(@RequestHeader("token")String token, String sn, Double money){
        String memberSn = JwtUtil.getUserId(token);

        if(sn  == null || sn.equals("")){
            return ResultFactory.buildFailResult("接收人不存在");
        }

        if(money == null || money <= 0){
            return ResultFactory.buildFailResult("金额不正确");
        }
        String member_balance_move_sn = dataUtil.createSn(memberSn);

        Db db = new Db();
        String sql = "member_balance_move_proc(?, ?, ?, ?, ?, ?)";
        List parameter = new ArrayList<Object>(){{
            add(member_balance_move_sn);
            add(memberSn);
            add(sn);
            add(money);
            add(0);
            add("余额转让");
        }};
        int res = (int) db.executeCall(sql, parameter, 1);
        System.out.println(res);
        if(res == 0){
            return ResultFactory.buildFailResult("操作失败");
        }
        if(res == -1){
            return ResultFactory.buildFailResult("转出或者转入的账号为空");
        }
        if(res == -2){
            return ResultFactory.buildFailResult("金额输入有误");
        }
        if(res == -3){
            return ResultFactory.buildFailResult("余额不足");
        }
        if(res == -4){
            return ResultFactory.buildFailResult("转出或者转入账号有误");
        }

        return ResultFactory.buildSuccessResult(new HashMap());

    }

    //余额转让记录
    @GetMapping("balanceMoveLog")
    public Result balanceMoveLog(@RequestHeader("token")String token, Integer limit, Integer page) {
        String memberSn = JwtUtil.getUserId(token);
        if(limit == null ){
            limit = 100;
        }
        if(page == null ){
            page = 1;
        }
        int offset = (page - 1) * limit;
        Db db = new Db();

        String sql = "select temp_table.*,member.avatar,member.nickname from (select id,sn,create_time,in_member_balance as balance,in_member_sn as member_sn,out_member_sn as other_sn,move_money from member_balance_move where in_member_sn = ? union all select id,sn,create_time,out_member_balance as balance,out_member_sn as member_sn,in_member_sn as other_sn,-move_money from member_balance_move where out_member_sn = ? ) as temp_table left join member on member.sn = temp_table.other_sn order by temp_table.create_time desc OFFSET ? ROWS FETCH NEXT ?  ROWS ONLY";

        List parameter = new ArrayList<Object>(){{
            add(memberSn);
            add(memberSn);
            add(offset);
        }};
        parameter.add(limit);
        List<Map> list = db.executeQueryBySql(sql, parameter);

        System.out.println(list);

        for(int i = 0; i< list.size(); i ++){
            Map log = list.get(i);
            String create_time = dataUtil.stampToDate((long) log.get("create_time"));
            log.put("_create_time",create_time);
            log.put("move_money", dataUtil.formatMoney(log.get("move_money"),2));
            log.put("balance", dataUtil.formatMoney(log.get("balance"),2));
            list.set(i,log);
        }

        String count_sql = "select count(*) as count from (select id,sn,create_time,in_member_balance as balance,in_member_sn as member_sn,out_member_sn as other_sn,move_money from member_balance_move where in_member_sn = ? union all select id,sn,create_time,out_member_balance as balance,out_member_sn as member_sn,in_member_sn as other_sn,-move_money from member_balance_move where out_member_sn = ? ) as temp_table left join member on member.sn = temp_table.other_sn";
        List parameter1 = new ArrayList<Object>(){{
            add(memberSn);
            add(memberSn);
        }};
        List<Map> count_list = db.executeQueryBySql(count_sql,parameter1);
        int count = (int) count_list.get(0).get("count");

        JSONObject data = new JSONObject();
        data.put("count",count);
        data.put("list",list);
        return ResultFactory.buildSuccessResult(data);


    }

}
