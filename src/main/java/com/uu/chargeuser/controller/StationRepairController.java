package com.uu.chargeuser.controller;

import com.alibaba.fastjson.JSONObject;
import com.uu.chargeuser.extend.Db;
import com.uu.chargeuser.result.Result;
import com.uu.chargeuser.result.ResultFactory;
import com.uu.chargeuser.utils.JwtUtil;
import com.uu.chargeuser.utils.dataUtil;
import net.sf.json.JSONArray;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * (StationRepair)表控制层
 *
 * @author makejava
 * @since 2021-03-08 15:32:05
 */
@RestController
@RequestMapping("stationRepair")
public class StationRepairController {

    //获取设备保修选项
    @GetMapping("repairOption")
    public Result repairOption(String station_sn){
        if(station_sn == null || station_sn.equals("")){
            return ResultFactory.buildFailResult("电站编号有误");
        }

        Db db = new Db();
        List parameter = new ArrayList<Object>(){{
            add(station_sn);
        }};
        String sql = "select telephone,station_repair_options from platform left join station on station.platform_sn = platform.sn where station.sn = ?";
        List<Map> resultMap = db.executeQueryBySql(sql,parameter);
        Map platform = resultMap.get(0);
        String[] repairOptionArr = platform.get("station_repair_options").toString().split("\\\n");
        JSONObject data = new JSONObject();
        data.put("telephone",platform.get("telephone"));
        data.put("station_repair_options",repairOptionArr);
        return ResultFactory.buildSuccessResult(data);
    }



    //提交保修信息
    @PostMapping("repairSubmit")
    public Result repairSubmit(HttpServletRequest request,String socket_number, String station_sn, String content, String mobile, String[] images){
        String memberSn = JwtUtil.getUserId(request.getHeader("token"));

        if (socket_number == null || socket_number.equals("")) {
            return ResultFactory.buildFailResult("插座号有误");
        }
        if (station_sn == null || station_sn.equals("")) {
            return ResultFactory.buildFailResult("电站编号有误");
        }
        if (content == null || content.equals("")) {
            return ResultFactory.buildFailResult("请选择故障内容");
        }
        if (mobile == null || mobile.equals("")) {
            return ResultFactory.buildFailResult("请输入手机号");
        }

        if(!dataUtil.isMobile(mobile)){
            return ResultFactory.buildFailResult("手机号码不正确");
        }


        Db db = new Db();
        Map station = db.table("station").where(new ArrayList(){{
            add(new ArrayList(){{
                add("sn");
                add("=");
                add(station_sn);
            }});
        }}).find("operator_member_sn,project_sn");
        String operator_member_sn = (String) station.get("operator_member_sn");
        String project_sn = station.get("project_sn").toString();

        JSONArray imagesJson = JSONArray.fromObject(images);
        String imagesJsonStr = imagesJson.toString();
        //System.out.println(imagesJson);

        long create_time = System.currentTimeMillis();

        Map insertData = new HashMap(){
            {
                put("socket_number", socket_number);
                put("station_sn", station_sn);
                put("project_sn", project_sn);
                put("operator_member_sn", operator_member_sn);
                put("member_sn", memberSn);
                put("mobile", mobile);
                put("content", content);
                put("images", imagesJsonStr);
                put("state", 0);
                put("create_time", create_time);
                put("update_time", create_time);
            }
        };

        //System.out.println(insertData);

        int res = db.table("station_repair").insert(insertData);
        //System.out.println(db.getLastSql());
        if (res > 0) {
            return ResultFactory.buildSuccessResult(insertData);
        }else{
            return ResultFactory.buildFailResult("失败");
        }

    }

    //报修记录
    @GetMapping("list")
    public Result list(HttpServletRequest request, Integer limit, Integer page){
        String memberSn = JwtUtil.getUserId(request.getHeader("token"));
        if(limit == null ){
            limit = 100;
        }
        if(page == null ){
            page = 1;
        }
        int offset = (page - 1) * limit;
        Db db = new Db();
        String list_sql = "select station_repair.state,station_repair.id,station.name as station_name,project.name as project_name,station_repair.station_sn,station_repair.socket_number,station_repair.create_time from station_repair left join station on station.sn = station_repair.station_sn left join project on project.sn = station.project_sn where station_repair.member_sn = ? order by station_repair.create_time desc OFFSET ? ROWS FETCH NEXT ?  ROWS ONLY";
        List parameter = new ArrayList<Object>(){{
            add(memberSn);
            add(offset);
        }};
        parameter.add(limit);
        List<Map> list = db.executeQueryBySql(list_sql,parameter);
        for(int i = 0; i< list.size(); i ++){
            Map repair = list.get(i);

            String create_time = dataUtil.stampToDate((long) repair.get("create_time"));
            repair.put("_create_time",create_time);

            list.set(i,repair);
        }

        String count_sql = "SELECT count(*) as count from station_repair  where member_sn = ?";
        List parameter1 = new ArrayList<Object>(){{
            add(memberSn);
        }};
        List<Map> count_list = db.executeQueryBySql(count_sql,parameter1);
        int count = (int) count_list.get(0).get("count");

        JSONObject data = new JSONObject();
        data.put("count",count);
        data.put("list",list);
        return ResultFactory.buildSuccessResult(data);
    }

    //报修详情
    @GetMapping("detail")
    public Result detail(HttpServletRequest request,int id){
        String memberSn = JwtUtil.getUserId(request.getHeader("token"));
        Db db = new Db();

        List parameter = new ArrayList<Object>(){{
            add(id);
            add(memberSn);
        }};
        String sql = "select station_repair.state,station_repair.id,station_repair.mobile,station_repair.images,station_repair.content,station_repair.handle_time,station_repair.handle,station.name as station_name,project.name as project_name,station_repair.station_sn,station_repair.socket_number,station_repair.create_time from station_repair left join station on station.sn = station_repair.station_sn left join project on project.sn = station.project_sn where station_repair.id = ? and station_repair.member_sn = ?";
        List list = db.executeQueryBySql(sql, parameter);
        Map detail = (Map) list.get(0);

        JSONArray imagesArr = JSONArray.fromObject(detail.get("images"));
        for (int i = 0; i < imagesArr.size(); i++) {
            imagesArr.set(i, request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + "/" + imagesArr.get(i));
        }
        detail.put("_images",imagesArr);
        String create_time = dataUtil.stampToDate((long) detail.get("create_time"));
        detail.put("_create_time",create_time);
        String handle_time = dataUtil.stampToDate((long) detail.get("handle_time"));
        detail.put("_handle_time",handle_time);

        return ResultFactory.buildSuccessResult(detail);
    }

}
