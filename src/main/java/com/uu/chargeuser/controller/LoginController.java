package com.uu.chargeuser.controller;

import com.alibaba.fastjson.JSONObject;
import com.uu.chargeuser.common.Constant;
import com.uu.chargeuser.entity.Member;
import com.uu.chargeuser.extend.Db;
import com.uu.chargeuser.extend.Wechat;
import com.uu.chargeuser.result.Result;
import com.uu.chargeuser.result.ResultFactory;
import com.uu.chargeuser.service.impl.MemberServiceImpl;
import com.uu.chargeuser.utils.JwtUtil;
import com.uu.chargeuser.utils.dataUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Validated
@RequestMapping("/login")
public class LoginController {
    private Logger logger = LoggerFactory.getLogger(LoginController.class);
    @Autowired
    MemberServiceImpl memberServiceImpl;

    @PostMapping(value = "/login",name = "用户登录")
    public Result weChatLogin(String sn, String password) throws UnsupportedEncodingException {
        logger.info("进入login");
        //用户信息
        Member member = memberServiceImpl.queryById(sn);
        logger.info("login_取出，登录人员信息："+member.toString());
        // 判断账号是否被冻结
        if (member.getState()==null|| -1==member.getState()){
            System.out.println("该账户已冻结，请联系管理员！");
            return ResultFactory.buildResult(400,"该账户已冻结，请联系管理员！",null);
        }
        //账号不存在、密码错误
        if (member == null || !member.getPassword().equals(password)) {
            return ResultFactory.buildResult(400,"账号或密码有误",null);
        } else {
            return ResultFactory.buildSuccessResult(JwtUtil.sign(member.getSn(), Constant.TOKEN_SECRET));
        }
    }
    @RequestMapping(path = "/401")
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public Result unauthorized() {
        return ResultFactory.buildResult(401,"无权限访问",null);
    }

    //微信登录
    @PostMapping("/wechat")
    public Result wechat(String code, String appid) throws UnsupportedEncodingException {

        if(code == null || code.equals("")){
            return ResultFactory.buildFailResult("code有误");
        }
        if(appid == null || appid.equals("")){
            return ResultFactory.buildFailResult("平台编号有误");
        }

        Db db = new Db();

        Map platform = db.table("platform").where(new ArrayList(){{
            add(new ArrayList(){{
                add("wechat_appid");
                add("=");
                add(appid);
            }});
        }}).find("sn,wechat_appid,wechat_appsecret,new_member_free_money");

        String platform_sn = platform.get("sn").toString();
        String secret = platform.get("wechat_appsecret").toString();
        JSONObject userInfo = Wechat.getUserInfo(code, appid, secret);

        if(!userInfo.containsKey("openid")){
            return ResultFactory.buildFailResult("获取用户信息失败");
        }

        //判断会员是否存在
        List where = new ArrayList(){{
            add(new ArrayList(){{
                add("wechat_openid");
                add("=");
                add(userInfo.getString("openid"));
            }});
            add(new ArrayList(){{
                add("platform_sn");
                add("=");
                add(platform_sn);
            }});
        }};
        List<Map> hasMembers = db.table("member").where(where).select("sn");

        long create_time = System.currentTimeMillis();
        String last_login_ip = dataUtil.getIpAddr();
        String token = "";
        String sn = "";
        int res = 0;
        Map updateData = new HashMap();
        updateData.put("avatar",userInfo.get("headimgurl"));
        updateData.put("nickname",userInfo.get("nickname"));
        updateData.put("sex",userInfo.get("sex"));
        updateData.put("province",userInfo.get("province"));
        updateData.put("city",userInfo.get("city"));
        updateData.put("country",userInfo.get("country"));
        updateData.put("last_login_time",create_time);
        updateData.put("last_login_ip",last_login_ip);

        if(userInfo.get("unionid") != null){
            updateData.put("wechat_unionid",userInfo.get("unionid"));
        }

        if(hasMembers.size() == 1){//如果用户存在：修改
            res = db.table("member").where(where).update(updateData);
            sn = hasMembers.get(0).get("sn").toString();
        }else if(hasMembers.size() == 0){//如果用户不存在：新建
            sn = dataUtil.createMemberSn();
            String operator_member_sn = "";
            String project_sn = "";
            updateData.put("sn",sn);
            updateData.put("wechat_openid",userInfo.get("openid"));
            updateData.put("platform_sn",platform_sn);
            updateData.put("type","1");
            updateData.put("state","1");
            updateData.put("role","1");
            updateData.put("operator_member_sn",operator_member_sn);
            updateData.put("project_sn",project_sn);
            updateData.put("create_time",create_time);
            res = db.table("member").insert(updateData);
            //若平台有新用户赠送金额
            if(true){
                String sql = "member_balance_change_proc(?, ?, ?, ?, ?, ?)";
                List parameter = new ArrayList<Object>();
                parameter.add(sn);
                parameter.add(7); //场景
                parameter.add(0); //实际金额
                parameter.add(platform.get("new_member_free_money"));  //赠送金额
                parameter.add("新会员赠送");  //备注
                parameter.add("");  //相关单号
                db.executeCall(sql, parameter, 1);
            }

        }else{
            return ResultFactory.buildFailResult("未识别会员，请联系管理员");
        }

        if(res > 0){
            token = JwtUtil.sign(sn, Constant.TOKEN_SECRET);
        }
        JSONObject data = new JSONObject();
        data.put("token",token);
        return ResultFactory.buildSuccessResult(data);
    }
}
