package com.uu.chargeuser.controller;

import com.alibaba.fastjson.JSONObject;
import com.uu.chargeuser.extend.Db;
import com.uu.chargeuser.result.Result;
import com.uu.chargeuser.result.ResultFactory;
import com.uu.chargeuser.utils.JwtUtil;
import com.uu.chargeuser.utils.dataUtil;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * (Member)表控制层
 *
 * @author makejava
 * @since 2021-03-09 14:43:09
 */
@RestController
@RequestMapping("member")
public class MemberController {

    //用户信息
    @GetMapping("detail")
    public Result detail(HttpServletRequest request){
        String memberSn = JwtUtil.getUserId(request.getHeader("token"));

        Db db = new Db();

        String sql = "select member.sn,member.nickname,member.avatar,member.balance,member.actual_balance,member.discount_balance,member.mobile,member.subscribe,member.one_charge_minuts,member.cancel_noload_check,member.role,member.state,member.type,member.platform_sn,member.project_sn,member.operator_member_sn,project.name as project_name from member left join project on project.sn = member.project_sn where member.sn = ?";
        List parameter = new ArrayList<Object>(){{
            add(memberSn);
        }};
        List list = db.executeQueryBySql(sql, parameter);
        Map detail = (Map) list.get(0);

        detail.put("balance", dataUtil.formatMoney(detail.get("balance"),2));
        detail.put("actual_balance", dataUtil.formatMoney(detail.get("actual_balance"),2));
        detail.put("discount_balance", dataUtil.formatMoney(detail.get("discount_balance"),2));


        detail.put("one_charge_hours", (int)((int)detail.get("one_charge_minuts") / 60));

        return ResultFactory.buildSuccessResult(detail);

    }

    //修改单次最大充电时长
    @PostMapping("editOneChargeHours")
    public Result editOneChargeHours(HttpServletRequest request,int hours){
        String memberSn = JwtUtil.getUserId(request.getHeader("token"));

        if(hours <= 0){
            return ResultFactory.buildFailResult("请输入大于0的小时数");
        }


        int one_charge_minuts = hours * 60;

        Db db = new Db();
        int res = db.table("member").where(new ArrayList() {{
            add(new ArrayList() {{
                add("sn");
                add("=");
                add(memberSn);
            }});
        }}).update(new HashMap() {{
            put("one_charge_minuts", one_charge_minuts);
        }});
        if(res > 0){
            return ResultFactory.buildSuccessResult(new JSONObject());
        }else{
            return ResultFactory.buildFailResult("失败");
        }
    }

    //设置手机号
    @PostMapping("setMobile")
    public Result setMobile(@RequestHeader("token")String token, String mobile, String code){
        String memberSn = JwtUtil.getUserId(token);

        if(mobile  == null || mobile.equals("")){
            return ResultFactory.buildFailResult("请输入手机号");
        }
        if(code  == null || code.equals("")){
            return ResultFactory.buildFailResult("请输入验证码");
        }

        Db db = new Db();

        Map verification_code = db.table("verification_code").where(new ArrayList() {{
            add(new ArrayList() {{
                add("member_sn");
                add("=");
                add(memberSn);
            }});
            add(new ArrayList() {{
                add("mobile");
                add("=");
                add(mobile);
            }});
            add(new ArrayList() {{
                add("scene");
                add("=");
                add(1);
            }});
            add(new ArrayList() {{
                add("create_time");
                add(">");
                add(System.currentTimeMillis() - (10 * 60 * 1000));
            }});
        }}).order("create_time desc").find("*");

        if(verification_code.size() == 0){
            return ResultFactory.buildFailResult("验证码有误");
        }
        if(!verification_code.get("code").equals(code)){
            return ResultFactory.buildFailResult("验证码有误");
        }
        if(!verification_code.get("state").toString().equals("0")){
            return ResultFactory.buildFailResult("验证码有误");
        }

        String verification_code_id = verification_code.get("id").toString();
        //验证通过 修改验证码状态
        db.table("verification_code").where(new ArrayList() {{
            add(new ArrayList() {{
                add("id");
                add("=");
                add(verification_code_id);
            }});
        }}).update(new HashMap(){{
            put("state", "1");
        }});

        //修改手机号
        int res = db.table("member").where(new ArrayList() {{
            add(new ArrayList() {{
                add("sn");
                add("=");
                add(memberSn);
            }});
        }}).update(new HashMap() {{
            put("mobile", mobile);
        }});
        if(res > 0){
            return ResultFactory.buildSuccessResult(new JSONObject());
        }else{
            return ResultFactory.buildFailResult("失败");
        }
    }


    @PostMapping("getMemberByKeywords")
    public Result getMemberByKeywords(@RequestHeader("token")String token,String keywords){
        String memberSn = JwtUtil.getUserId(token);

        if(keywords  == null || keywords.equals("")){
            return ResultFactory.buildFailResult("请输入账号或者手机号");
        }

        Db db = new Db();
        Map detail = db.table("member").where(new ArrayList(){{
            add(new ArrayList(){{
                add("sn");
                add("=");
                add(keywords);
            }});
            add(new ArrayList(){{
                add("mobile");
                add("=");
                add(keywords);
                add("OR");
            }});

        }}).find("avatar,sn,nickname,mobile");
        if(detail.size() == 0){
            return ResultFactory.buildFailResult("此会员不存在");
        }
        if(memberSn.equals(detail.get("sn"))){
            return ResultFactory.buildFailResult("不能转让给自己");
        }

        return ResultFactory.buildSuccessResult(detail);

    }







}
