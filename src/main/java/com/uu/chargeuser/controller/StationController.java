package com.uu.chargeuser.controller;

import com.alibaba.fastjson.JSONObject;
import com.uu.chargeuser.extend.Db;
import com.uu.chargeuser.result.Result;
import com.uu.chargeuser.result.ResultFactory;
import com.uu.chargeuser.utils.JwtUtil;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * (Station)表控制层
 *
 * @author makejava
 * @since 2021-03-06 12:12:22
 */
@RestController
@RequestMapping("station")
public class StationController {

    //常用电站列表
    @GetMapping("usedStation")
    public Result usedStation(HttpServletRequest request){
        String memberSn = JwtUtil.getUserId(request.getHeader("token"));

        Db db = new Db();
        String list_sql = "select station.sn,station.name as station_name,project.sn as project_sn,project.name as project_name,is_online,(select count(*) from socket where state = 1 and station_sn = station.sn) as free_socket_count from station left join project on station.project_sn = project.sn where station.sn in (select  station_sn from charge_log where member_sn = ? ) order by station.is_online desc OFFSET 0 ROWS FETCH NEXT 100  ROWS ONLY";
        List parameter = new ArrayList<Object>(){{
            add(memberSn);
        }};
        List<Map> list = db.executeQueryBySql(list_sql,parameter);
        for (int i = 0; i < list.size(); i++) {
            Map station = list.get(i);

            String station_sn = (String) station.get("sn");
            List where  = new ArrayList(){{
                add(new ArrayList<Object>(){{
                    add("station_sn");
                    add("=");
                    add(station_sn);
                }});
            }};
            List socket_list = db.table("socket").where(where).order("socket_number asc").select("sn,socket_number,state");

            station.put("socket_list",socket_list);
            list.set(i,station);
        }

        int count = (int) list.size();

        JSONObject data = new JSONObject();
        data.put("count",count);
        data.put("list",list);
        return ResultFactory.buildSuccessResult(data);
    }

    //附近电站列表
    @GetMapping("nearList")
    public Result nearList(String latitude, String longitude, String keywords){

        Db db = new Db();
        String keywords_sql = "";
        List parameter = new ArrayList<Object>(){{
            add(latitude);
            add(longitude);
        }};
        if(keywords != null && !keywords.equals("")){
            parameter.add("%" + keywords + "%");
            parameter.add("%" + keywords + "%");
            parameter.add(keywords);
            keywords_sql = "  and  ( station.name like ? OR project.name like ? OR station.sn = ?) ";
        }
        String list_sql = "SELECT * FROM ( SELECT station.address,station.sn as station_sn,station.name as station_name,station.is_online,station.latitude,station.longitude,project.sn as project_sn,project.name as project_name,(select count(*) from socket where state = 1 and station_sn = station.sn) as free_socket_count,(select count(*) from socket where state = 2 and station_sn = station.sn) as used_socket_count,CAST(dbo.GetDistance(?,?,station.latitude,station.longitude) AS NUMERIC(10,2)) AS distance FROM station LEFT JOIN project ON project.sn = station.project_sn WHERE station.state = 1 and station.project_sn is not null and station.operator_member_sn is not null " + keywords_sql + " ) AS ms WHERE distance < 20 ORDER BY distance ASC";
        List<Map> list = db.executeQueryBySql(list_sql,parameter);
        //System.out.println(db.getLastSql());

        int count = (int) list.size();

        JSONObject data = new JSONObject();
        data.put("count",count);
        data.put("list",list);
        return ResultFactory.buildSuccessResult(data);
    }

    //电站详情
    @GetMapping("detail")
    public Result detail(String sn){

        Db db = new Db();

        String list_sql = "select station.sn,station.name as station_name,project.sn as project_sn,project.name as project_name,is_online,(select count(*) from socket where state = 1 and station_sn = station.sn) as free_socket_count from station left join project on station.project_sn = project.sn where station.sn = ? ";
        List parameter = new ArrayList<Object>(){{
            add(sn);
        }};
        List<Map> list = db.executeQueryBySql(list_sql,parameter);
        System.out.println(list);
        if(list.size() == 0){
            return ResultFactory.buildFailResult("电站不存在");
        }
        Map station = list.get(0);

        String station_sn = sn;
        List where  = new ArrayList(){{
            add(new ArrayList<Object>(){{
                add("station_sn");
                add("=");
                add(station_sn);
            }});
        }};
        List socket_list = db.table("socket").where(where).order("socket_number asc").select("sn,socket_number,state");

        station.put("socket_list",socket_list);

        return ResultFactory.buildSuccessResult(station);
    }

}
