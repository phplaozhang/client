package com.uu.chargeuser.controller;

import com.uu.chargeuser.extend.Db;
import com.uu.chargeuser.result.Result;
import com.uu.chargeuser.result.ResultFactory;
import com.uu.chargeuser.utils.JwtUtil;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("wechat")
public class Wechat {

    @PostMapping("getJsSdkSignPackage")
    public Result getJsSdkSignPackage(@RequestHeader("token")String token, String url){
        String memberSn = JwtUtil.getUserId(token);

        Db db = new Db();

        String sql = "select member.sn,platform.wechat_appid,platform.wechat_appsecret from member left join platform on platform.sn = member.platform_sn where member.sn = ?";
        List parameter = new ArrayList<Object>(){{
            add(memberSn);
        }};
        List list = db.executeQueryBySql(sql, parameter);
        if(list.size() == 0){
            return ResultFactory.buildFailResult("会员信息不存在");
        }
        Map detail = (Map) list.get(0);

        String appid = detail.get("wechat_appid").toString();
        String secret = detail.get("wechat_appsecret").toString();
        Map jsSdkSignPackage = com.uu.chargeuser.extend.Wechat.getJsSdkSignPackage(appid, secret, url);

        return ResultFactory.buildSuccessResult(jsSdkSignPackage);
    }
}
