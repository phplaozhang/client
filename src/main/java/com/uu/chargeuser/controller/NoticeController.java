package com.uu.chargeuser.controller;

import com.alibaba.fastjson.JSONObject;
import com.uu.chargeuser.extend.Db;
import com.uu.chargeuser.result.Result;
import com.uu.chargeuser.result.ResultFactory;
import com.uu.chargeuser.utils.JwtUtil;
import com.uu.chargeuser.utils.dataUtil;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * (Notice)表控制层
 *
 * @author makejava
 * @since 2021-03-09 15:10:57
 */
@RestController
@RequestMapping("notice")
public class NoticeController {

    @GetMapping("list")
    public Result list(HttpServletRequest request, int limit, int page){
        String memberSn = JwtUtil.getUserId(request.getHeader("token"));

        int offset = (page - 1) * limit;

        Db db = new Db();
        String list_sql = "SELECT id,name,thumb,link,introduction,create_time,( select count(*) from notice_view_log where notice_id = notice.id and member_sn = ? ) as is_read from notice where target = 1 order by create_time desc OFFSET ? ROWS FETCH NEXT ?  ROWS ONLY";
        List parameter = new ArrayList<Object>(){{
            add(memberSn);
            add(offset);
            add(limit);
        }};
        List<Map> list = db.executeQueryBySql(list_sql,parameter);
        //System.out.println(db.getLastSql());

        String count_sql = "SELECT count(*) as count from notice where target = 1 ";
        List parameter1 = new ArrayList<Object>();
        List<Map> count_list = db.executeQueryBySql(count_sql,parameter1);
        int count = (int) count_list.get(0).get("count");

        for(int i = 0; i< list.size(); i ++){
            Map charge_log = list.get(i);

            String create_time = dataUtil.stampToDate((long) charge_log.get("create_time"));
            charge_log.put("_create_time",create_time);

            charge_log.put("_create_date_str",create_time.substring(0,10));
            charge_log.put("_create_time_str",create_time.substring(11,16));

            list.set(i,charge_log);
        }
        JSONObject data = new JSONObject();
        data.put("count",count);
        data.put("list",list);
        return ResultFactory.buildSuccessResult(data);
    }

    @GetMapping("detail")
    public Result detail(HttpServletRequest request,int id){
        String memberSn = JwtUtil.getUserId(request.getHeader("token"));
        Db db = new Db();

        List where  = new ArrayList(){{
            add(new ArrayList<Object>(){{
                add("id");
                add("=");
                add(id);
            }});
        }};

        Map detail = db.table("notice").where(where).find("*");

        String create_time = dataUtil.stampToDate((long) detail.get("create_time"));
        detail.put("_create_time",create_time);


        //新增阅读记录
        int isRead = db.table("notice_view_log").where(new ArrayList(){{
            add(new ArrayList(){{
                add("notice_id");
                add("=");
                add(id);
            }});
            add(new ArrayList(){{
                add("member_sn");
                add("=");
                add(memberSn);
            }});
        }}).count("*");
        if(isRead == 0){
            long insert_create_time = System.currentTimeMillis();
            Map insert = new HashMap(){
                {
                    put("notice_id", id);
                    put("member_sn", memberSn);
                    put("create_time", insert_create_time);
                    put("update_time", insert_create_time);
                }
            };
            db.table("notice_view_log").insert(insert);
        }


        return ResultFactory.buildSuccessResult(detail);
    }

    //公告未读数量
    @GetMapping("noReadNum")
    public Result noReadNum(HttpServletRequest request){
        String memberSn = JwtUtil.getUserId(request.getHeader("token"));
        Db db = new Db();

        String sql = "select count(*) as count from notice where id not in (select notice_id from notice_view_log where member_sn = ?)";
        List<Map> count_list = db.executeQueryBySql(sql, new ArrayList<Object>(){{
            add(memberSn);
        }});
        int count = (int) count_list.get(0).get("count");

        JSONObject data = new JSONObject();
        data.put("count",count);
        return ResultFactory.buildSuccessResult(data);
    }


}
