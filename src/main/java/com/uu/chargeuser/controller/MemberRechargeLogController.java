package com.uu.chargeuser.controller;

import com.alibaba.fastjson.JSONObject;
import com.github.wxpay.sdk.WXPayConfig;
import com.uu.chargeuser.extend.Db;
import com.uu.chargeuser.extend.Wechat;
import com.uu.chargeuser.result.Result;
import com.uu.chargeuser.result.ResultFactory;
import com.uu.chargeuser.utils.JwtUtil;
import com.uu.chargeuser.utils.dataUtil;
import net.sf.json.JSONArray;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * (MemberRechargeLog)表控制层
 *
 * @author makejava
 * @since 2021-03-08 15:12:01
 */
@RestController
@RequestMapping("memberRechargeLog")
public class MemberRechargeLogController {


    //充值金额选项
    @GetMapping("rechargeMoney")
    public Result rechargeMoney(HttpServletRequest request){
        String memberSn = JwtUtil.getUserId(request.getHeader("token"));

        Db db = new Db();

        Map memberBalance = db.table("member").where(new ArrayList(){{
            add(new ArrayList<Object>(){{
                add("sn");
                add("=");
                add(memberSn);
            }});
        }}).find("balance,actual_balance,discount_balance,project_sn");

        Object projectSn = memberBalance.get("project_sn");

        memberBalance.put("balance", dataUtil.formatMoney(memberBalance.get("balance"),2));
        memberBalance.put("actual_balance", dataUtil.formatMoney(memberBalance.get("actual_balance"),2));
        memberBalance.put("discount_balance", dataUtil.formatMoney(memberBalance.get("discount_balance"),2));
        //System.out.println(projectSn);

        List where  = new ArrayList(){{
            add(new ArrayList<Object>(){{
                add("project_sn");
                add("=");
                add(projectSn);
            }});
        }};

        List<Map> list = db.table("project_recharge_setting").where(where).order("actual_money asc").select("project_sn,actual_money,discount_money");

        for (int i = 0; i < list.size(); i++) {
            Map project_recharge_setting = list.get(i);
            double actual_money = Double.parseDouble(project_recharge_setting.get("actual_money").toString());
            double discount_money = Double.parseDouble(project_recharge_setting.get("discount_money").toString());
            project_recharge_setting.put("actual_money", dataUtil.formatMoney(actual_money,2));
            project_recharge_setting.put("discount_money", dataUtil.formatMoney(discount_money,2));
            project_recharge_setting.put("money", dataUtil.formatMoney(actual_money + discount_money,2));
            list.set(i,project_recharge_setting);
        }

        JSONObject data = new JSONObject();
        data.put("member_balance",memberBalance);
        data.put("list",list);
        return ResultFactory.buildSuccessResult(data);
    }


    //充值记录
    @GetMapping("list")
    public Result rechargeMoney(HttpServletRequest request,int limit, int page){
        String memberSn = JwtUtil.getUserId(request.getHeader("token"));
        Db db = new Db();

        List where  = new ArrayList(){{
            add(new ArrayList<Object>(){{
                add("member_sn");
                add("=");
                add(memberSn);
            }});
            add(new ArrayList<Object>(){{
                add("state");
                add("=");
                add(10);
            }});
        }};

        List<Map> list = db.table("member_recharge_log").where(where).order("create_time desc").limit(limit).page(page).select("id,member_sn,amount,discount_amount,pay_amount,member_balance,member_actual_balance,member_discount_balance,pay_type,state,pay_time,member_type");
        int count =  db.table("member_recharge_log").where(where).count("*");

        for(int i = 0; i< list.size(); i ++){
            Map member_recharge_log = list.get(i);

            member_recharge_log.put("member_balance", dataUtil.formatMoney(member_recharge_log.get("member_balance"),2));
            member_recharge_log.put("amount", dataUtil.formatMoney(member_recharge_log.get("amount"),2));
            member_recharge_log.put("discount_amount", dataUtil.formatMoney(member_recharge_log.get("discount_amount"),2));
            member_recharge_log.put("pay_amount", dataUtil.formatMoney(member_recharge_log.get("pay_amount"),2));

            String pay_time = dataUtil.stampToDate((long) member_recharge_log.get("pay_time"));
            member_recharge_log.put("_pay_time",pay_time);

            list.set(i,member_recharge_log);
        }

        JSONObject data = new JSONObject();
        data.put("count",count);
        data.put("list",list);
        return ResultFactory.buildSuccessResult(data);
    }

    //充值
    @PostMapping("create")
    public Result create(@RequestHeader("token")String token, double money, int pay_type){
        String memberSn = JwtUtil.getUserId(token);
        double amount = 0;
        double pay_amount = 0;
        double discount_amount = 0;


        if(money <= 0){
            return ResultFactory.buildFailResult("金额有误");
        }

        Db db = new Db();

        String sql = "select member.wechat_openid,member.type,member.sn,member.platform_sn,member.project_sn,platform.recharge_config,platform.wechat_appid,platform.wechat_mchid,platform.wechat_key,platform.wechat_appsecret,platform.alipay_appid,platform.ailpay_private_key from member left join platform on platform.sn = member.platform_sn where member.sn = ?";
        List parameter = new ArrayList<Object>(){{
            add(memberSn);
        }};
        List memberList = db.executeQueryBySql(sql, parameter);
        if(memberList.size() == 0){
            return ResultFactory.buildFailResult("会员信息有误");
        }
        Map member = (Map) memberList.get(0);


        Object projectSn = member.get("project_sn");


        List where  = new ArrayList(){{
            add(new ArrayList<Object>(){{
                add("project_sn");
                add("=");
                add(projectSn);
            }});
        }};

        List<Map> list = db.table("project_recharge_setting").where(where).order("actual_money asc").select("project_sn,actual_money,discount_money");

        if(list.size() > 0){ //如果小区有设置充值配置
            for (int i = 0; i < list.size(); i++) {
                Map project_recharge_setting = list.get(i);
                double actual_money = Double.parseDouble(project_recharge_setting.get("actual_money").toString());
                double discount_money = Double.parseDouble(project_recharge_setting.get("discount_money").toString());
                if(money == actual_money){
                    discount_amount = discount_money;
                    pay_amount = actual_money;
                    break;
                }
            }
        }else{//如果小区没有有设置充值配置，则用平台
            JSONArray rechargeConfigArr = JSONArray.fromObject(member.get("recharge_config"));
            for (Object o : rechargeConfigArr) {
                Map rechargeConfig = (Map) o;
                double actual_money = Double.parseDouble(rechargeConfig.get("actual_money").toString());
                double discount_money = Double.parseDouble(rechargeConfig.get("discount_money").toString());
                if(money == actual_money){
                    discount_amount = discount_money;
                    pay_amount = actual_money;
                    break;
                }
            }
        }


        if(pay_amount == 0.0){
            return ResultFactory.buildFailResult("金额有误.");
        }
        amount = pay_amount + discount_amount;
        long create_time = System.currentTimeMillis();
        String chargeLogSn = dataUtil.createSn(memberSn);
        //新增充电记录
        Map insertData = new HashMap();
        insertData.put("sn", chargeLogSn);
        insertData.put("trade_no", "");
        insertData.put("member_sn", memberSn);
        insertData.put("project_sn", projectSn);
        insertData.put("member_type", member.get("type"));
        insertData.put("amount", amount);
        insertData.put("pay_amount", pay_amount);
        insertData.put("discount_amount", discount_amount);
        insertData.put("pay_type", pay_type);
        insertData.put("state", 1);
        insertData.put("create_time", create_time);

        //System.out.println(insertData);
        int createRes = db.table("member_recharge_log").insert(insertData);
        if(createRes < 1){
            return ResultFactory.buildFailResult("操作失败");
        }
        Map resultmap = new HashMap();

        if(pay_type == 1){ //微信支付
            //组装数据
            String wechat_appid = member.get("wechat_appid").toString();
            String wechat_mchid = member.get("wechat_mchid").toString();
            String wechat_key = member.get("wechat_key").toString();
            String wechat_appsecret = member.get("wechat_appsecret").toString();
            //微信支付配置
            WXPayConfig wxPayConfig = new WXPayConfig() {
                @Override
                public String getAppID() {
                    return wechat_appid;
                }

                @Override
                public String getMchID() {
                    return wechat_mchid;
                }

                @Override
                public String getKey() {
                    return wechat_key;
                }

                @Override
                public InputStream getCertStream() {
                    return null;
                }

                @Override
                public int getHttpConnectTimeoutMs() {
                    return 0;
                }

                @Override
                public int getHttpReadTimeoutMs() {
                    return 0;
                }
            };
            //订单数据
            Map params = new HashMap();
            params.put("body","会员充值");
            params.put("out_trade_no",chargeLogSn);
            params.put("openid",member.get("wechat_openid"));
            params.put("total_fee",((int) (pay_amount * 100)) + "");
            params.put("notify_url","http://www.baidu.com");

            try {
                resultmap = Wechat.wxUnifiedorder(wxPayConfig, params);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println(e.getMessage());
            }

        }else if(pay_type == 2){    //支付宝

        }

        if(resultmap.size() == 0){
            return ResultFactory.buildFailResult("请求支付信息失败");
        }

        if(resultmap.get("return_code").equals("FAIL")){
            return ResultFactory.buildFailResult(resultmap.get("return_msg").toString());
        }

        if(resultmap.get("result_code").equals("FAIL")){
            return ResultFactory.buildFailResult(resultmap.get("err_code_des").toString());
        }

        return ResultFactory.buildSuccessResult(resultmap);
    }
}
