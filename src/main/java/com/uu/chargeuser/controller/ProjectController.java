package com.uu.chargeuser.controller;

import com.alibaba.fastjson.JSONObject;
import com.uu.chargeuser.extend.Db;
import com.uu.chargeuser.result.Result;
import com.uu.chargeuser.result.ResultFactory;
import com.uu.chargeuser.utils.dataUtil;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * (Project)表控制层
 *
 * @author makejava
 * @since 2021-03-06 12:08:23
 */
@RestController
@RequestMapping("project")
public class ProjectController {

    //功率计费阶梯
    @GetMapping("chargingPrice")
    public Result chargingPrice(String sn){
        Db db = new Db();

        List where  = new ArrayList(){{
            add(new ArrayList<Object>(){{
                add("project_sn");
                add("=");
                add(sn);
            }});
        }};

        List<Map> list = db.table("project_charging_price").where(where).limit(99999).order("start_power asc").select("project_sn,start_power,end_power,money_hours");
        int count = list.size();

        for (int i = 0; i < list.size(); i++) {
            Map project_charging_price = list.get(i);
            double hour_price = 1 / Double.parseDouble(project_charging_price.get("money_hours").toString());
            project_charging_price.put("hour_price", dataUtil.formatMoney(hour_price,2));
            list.set(i,project_charging_price);
        }

        JSONObject data = new JSONObject();
        data.put("count",count);
        data.put("list",list);
        return ResultFactory.buildSuccessResult(data);
    }


}
