package com.uu.chargeuser.controller;


import com.alibaba.fastjson.JSONObject;
import com.uu.chargeuser.extend.Db;
import com.uu.chargeuser.extend.TencentSms;
import com.uu.chargeuser.result.Result;
import com.uu.chargeuser.result.ResultFactory;
import com.uu.chargeuser.utils.JwtUtil;
import com.uu.chargeuser.utils.dataUtil;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("sms")
public class SmsController {

    //发送验证码短信
    @PostMapping("sendCodeSms")
    public Result sendCodeSms(@RequestHeader("token")String token, String mobile, String scene){
        String memberSn = JwtUtil.getUserId(token);

        if(mobile  == null || mobile.equals("")){
            return ResultFactory.buildFailResult("请输入手机号");
        }
        if(scene  == null || scene.equals("")){
            return ResultFactory.buildFailResult("验证码场景有误");
        }

        Db db = new Db();
        Map member = db.table("member").where(new ArrayList(){{
            add(new ArrayList<Object>(){{
                add("sn");
                add("=");
                add(memberSn);
            }});
        }}).find("platform_sn,mobile");
        String platformSn = member.get("platform_sn").toString();

        if(mobile.equals(member.get("mobile").toString()) && scene.equals("1")){ //如果是原来手机号
            return ResultFactory.buildFailResult("此手机号已绑定");
        }

        int code = dataUtil.random(1000,9999);
        String codeStr = code + "";

        TencentSms tencentSms = new TencentSms(platformSn);
        boolean res = tencentSms.sendCode(mobile, codeStr);

        if(res){
            //插入数据库
            long create_time = System.currentTimeMillis();
            Map insert = new HashMap(){
                {
                    put("member_sn", memberSn);
                    put("mobile", mobile);
                    put("code", codeStr);
                    put("state", "0");
                    put("scene", scene);
                    put("create_time", create_time);
                    put("update_time", create_time);
                }
            };
            db.table("verification_code").insert(insert);

            JSONObject data = new JSONObject();
            data.put("mobile",mobile);
            data.put("scene",scene);
            return ResultFactory.buildSuccessResult(data);
        }else {
            return ResultFactory.buildFailResult("发送失败");
        }
    }


}
