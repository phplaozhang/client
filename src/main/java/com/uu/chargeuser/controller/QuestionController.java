package com.uu.chargeuser.controller;

import com.alibaba.fastjson.JSONObject;
import com.uu.chargeuser.extend.Db;
import com.uu.chargeuser.result.Result;
import com.uu.chargeuser.result.ResultFactory;
import com.uu.chargeuser.utils.dataUtil;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * (Question)表控制层
 *
 * @author makejava
 * @since 2021-03-08 10:34:03
 */
@RestController
@RequestMapping("question")
public class QuestionController {

    //常见问题列表
    @GetMapping("list")
    public Result list(int limit, int page){

        Db db = new Db();

        List<Map> list = db.table("question").order("create_time desc").limit(limit).page(page).select("id,name");
        int count =  db.table("question").count("*");

        JSONObject data = new JSONObject();
        data.put("count",count);
        data.put("list",list);
        return ResultFactory.buildSuccessResult(data);
    }

    //问题详情
    @GetMapping("detail")
    public Result detail(int id){
        Db db = new Db();

        List where  = new ArrayList(){{
            add(new ArrayList<Object>(){{
                add("id");
                add("=");
                add(id);
            }});
        }};

        Map detail = db.table("question").where(where).find("id,name,content,create_time");

        String create_time = dataUtil.stampToDate((long) detail.get("create_time"));
        detail.put("_create_time",create_time);

        return ResultFactory.buildSuccessResult(detail);
    }

}
