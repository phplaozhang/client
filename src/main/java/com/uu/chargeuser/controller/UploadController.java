package com.uu.chargeuser.controller;


import com.alibaba.fastjson.JSONObject;
import com.uu.chargeuser.result.Result;
import com.uu.chargeuser.result.ResultFactory;
import com.uu.chargeuser.utils.uploadUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;


@RestController
@RequestMapping("upload")
public class UploadController {

    //图片上传
    @RequestMapping("/uploadImage")
    public Result uploadImage(@RequestParam("file") MultipartFile file, HttpServletRequest request){

        uploadUtils uploadUtils = new uploadUtils();
        Map uploadResult = uploadUtils.uploadImage(file);

        JSONObject data = new JSONObject();
        if(uploadResult.get("code") == "200"){
            data.put("path",uploadResult.get("relativePath"));
            data.put("url",request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + "/" + uploadResult.get("relativePath"));
            return ResultFactory.buildSuccessResult(data);
        }else{
            return ResultFactory.buildFailResult((String) uploadResult.get("msg"));
        }


    }

}
