package com.uu.chargeuser.repository;

import com.uu.chargeuser.entity.Permission;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PermissionRepository extends JpaRepository<Permission,Integer> {
}
