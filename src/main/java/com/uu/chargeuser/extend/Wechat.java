package com.uu.chargeuser.extend;


import com.alibaba.fastjson.JSONObject;
import com.github.wxpay.sdk.WXPay;
import com.github.wxpay.sdk.WXPayConfig;
import com.github.wxpay.sdk.WXPayUtil;
import com.uu.chargeuser.result.ResultFactory;
import com.uu.chargeuser.utils.HttpUtil;
import com.uu.chargeuser.utils.JedisUtil;
import com.uu.chargeuser.utils.dataUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import redis.clients.jedis.Jedis;


import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.apache.commons.codec.digest.DigestUtils.*;
import static org.apache.commons.codec.digest.DigestUtils.sha1Hex;


public class Wechat {

    private String appid;
    private String secret;

    //获取用户信息
    public static JSONObject getUserInfo(String code, String appid, String secret){
        JSONObject useinfoJsonObject = new JSONObject();
                //获取access_token
        String url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=" + appid + "&secret=" + secret + "&code=" + code + "&grant_type=authorization_code";
        String res = HttpUtil.get(url, new HashMap<>());
        JSONObject jsonObject = JSONObject.parseObject(res);
        if (!jsonObject.containsKey("access_token")) {
            return useinfoJsonObject;
        }
        String access_token = jsonObject.getString("access_token");

        //获取用户信息
        String useinfoUrl = "https://api.weixin.qq.com/sns/userinfo?access_token=" + access_token + "&openid=OPENID&lang=zh_CN";
        String useinfoRes = HttpUtil.get(useinfoUrl, new HashMap<>());
        useinfoJsonObject = JSONObject.parseObject(useinfoRes);

        return useinfoJsonObject;
    }

    //微信支付统一下单
    public static Map<String, String> wxUnifiedorder(WXPayConfig config, Map<String, String> params) throws Exception {

        WXPay wxPay = new WXPay(config);

        params.put("body", params.get("body"));//商品描述
        params.put("out_trade_no", params.get("out_trade_no"));
        params.put("openid", params.get("openid"));
        params.put("total_fee", params.get("total_fee"));
        params.put("trade_type","JSAPI");
        params.put("notify_url",params.get("notify_url"));
        params.put("spbill_create_ip", dataUtil.getIpAddr());
        System.out.println(params);
        Map<String,String> resultMap = wxPay.unifiedOrder(params);

        return resultMap;

    }

    //支付回调
    public static String payNotify(WXPayConfig config, String notifyStr){
        String xmlBack = "<xml><return_code><![CDATA[FAIL]]></return_code><return_msg><![CDATA[报文为空]]></return_msg></xml> ";
        try {
            // 转换成map
            Map<String, String> resultMap = WXPayUtil.xmlToMap(notifyStr);
            WXPay wxpayApp = new WXPay(config);
            if (wxpayApp.isPayResultNotifySignatureValid(resultMap)) {
                String returnCode = resultMap.get("return_code");  //状态
                String outTradeNo = resultMap.get("out_trade_no");//商户订单号
                String transactionId = resultMap.get("transaction_id");
                if (returnCode.equals("SUCCESS")) {
                    if (StringUtils.isNotBlank(outTradeNo)) {
                        /**
                         * 注意！！！
                         * 请根据业务流程，修改数据库订单支付状态，和其他数据的相应状态
                         *
                         */
                        System.out.println(outTradeNo);
                        xmlBack = "<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>";
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return xmlBack;

    }

    /*
     * 获取access_token
     *  https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET
     */
    public static String getAccessToken(String appid, String secret){
        int create_time = (int) (System.currentTimeMillis() / 1000);
        //判断缓存是否存在
        Jedis jedis = JedisUtil.getJedis();
        //查询缓存中是否存在
        String redisKey = "access_token_" + appid;
        if(jedis.exists(redisKey)){
            String access_token_str = jedis.get(redisKey);
            JSONObject jsonObject = JSONObject.parseObject(access_token_str);
            if((int) jsonObject.get("expire_time") > create_time){
                JedisUtil.close(jedis);
                return jsonObject.get("access_token").toString();
            }
        }
        String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + appid + "&secret=" + secret;
        String res = HttpUtil.get(url, new HashMap<>());
        JSONObject jsonObject = JSONObject.parseObject(res);
        if (!jsonObject.containsKey("access_token")) {
            return "";
        }
        jsonObject.put("expire_time", (int)jsonObject.get("expires_in") + create_time);
        jedis.set(redisKey,jsonObject.toString());
        JedisUtil.close(jedis);
        return jsonObject.get("access_token").toString();
    }

    /*
     * 获取jsapi_ticket
     *  https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=ACCESS_TOKEN&type=jsapi
     */
    public static String getJsapiTicket(String access_token, String appid){
        int create_time = (int) (System.currentTimeMillis() / 1000);
        //判断缓存是否存在
        Jedis jedis = JedisUtil.getJedis();
        //查询缓存中是否存在
        String redisKey = "jsapi_ticket_" + appid;
        if(jedis.exists(redisKey)){
            String jsapi_ticket_str = jedis.get(redisKey);
            JSONObject jsonObject = JSONObject.parseObject(jsapi_ticket_str);
            if((int) jsonObject.get("expire_time") > create_time){
                JedisUtil.close(jedis);
                return jsonObject.get("ticket").toString();
            }
        }
        String url = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=" + access_token + "&type=jsapi";
        String res = HttpUtil.get(url, new HashMap<>());
        JSONObject jsonObject = JSONObject.parseObject(res);
        if (!jsonObject.containsKey("ticket")) {
            return "";
        }
        jsonObject.put("expire_time", (int)jsonObject.get("expires_in") + create_time);
        jedis.set(redisKey,jsonObject.toString());
        JedisUtil.close(jedis);
        return jsonObject.get("ticket").toString();
    }

    /*
     * 获取jsdk签名数据
     */
    public static Map getJsSdkSignPackage(String appid, String secret, String url){
        String accessToken = getAccessToken(appid, secret);
        String jsapiTicket = getJsapiTicket(accessToken, appid);
        String timestamp = Long.toString(System.currentTimeMillis() / 1000);
        String nonceStr = UUID.randomUUID().toString();
        String url1 = url;
        try {
            url1 = URLDecoder.decode(url,"UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        // 这里参数的顺序要按照 key 值 ASCII 码升序排序
        String string = "jsapi_ticket=" + jsapiTicket + "&noncestr=" + nonceStr + "&timestamp=" + timestamp + "&url=" + url;

        String signature = sha1Hex(string);

        HashMap signPackage = new HashMap();
        signPackage.put("appId", appid);
        signPackage.put("nonceStr", nonceStr);
        signPackage.put("timestamp", timestamp);

        signPackage.put("url", url1);
        signPackage.put("signature", signature);
        signPackage.put("rawString", string);

        return signPackage;
    }
}
