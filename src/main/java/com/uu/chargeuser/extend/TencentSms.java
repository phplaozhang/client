package com.uu.chargeuser.extend;


import com.alibaba.fastjson.JSONObject;
import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.exception.TencentCloudSDKException;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.common.profile.HttpProfile;
import com.tencentcloudapi.sms.v20190711.SmsClient;
import com.tencentcloudapi.sms.v20190711.models.SendSmsRequest;
import com.tencentcloudapi.sms.v20190711.models.SendSmsResponse;
import com.tencentcloudapi.sms.v20190711.models.SendStatus;
import net.sf.json.JSONArray;

import java.util.ArrayList;
import java.util.Map;

public class TencentSms {

    String AppID = "";
    String AppKey = "";
    String SecretId = "";
    String SecretKey = "";
    String Sign = "";
    String TemplateID = "";

    public TencentSms(String platform_sn){
        Db db = new Db();
        Map platform = db.table("platform").where(new ArrayList() {{
            add(new ArrayList() {{
                add("sn");
                add("=");
                add(platform_sn);
            }});
        }}).find("tencent_sms");
        JSONObject tencent_sms = JSONObject.parseObject(platform.get("tencent_sms").toString());

        AppID = tencent_sms.getString("AppID");
        AppKey = tencent_sms.getString("AppKey");
        SecretId = tencent_sms.getString("SecretId");
        SecretKey = tencent_sms.getString("SecretKey");
        Sign = tencent_sms.getString("Sign");
        TemplateID = tencent_sms.getString("TemplateID");

    }

    public boolean sendCode(String mobile, String code){

        mobile = "+86" + mobile;
        String[] mobiles = {mobile};
        String[] codes = {code};

        String sessionContext = "";
        SendSmsResponse resp = send(mobiles,codes,sessionContext);
        if(resp == null){
            return false;
        }
        SendStatus[] sendStatusSet = resp.getSendStatusSet();

        if(sendStatusSet[0].getFee() != null && sendStatusSet[0].getFee() > 0 ){
            return true;
        }else{
            return false;
        }
    }

    public SendSmsResponse send(String[] mobile, String[] templateParam, String sessionContext){
        SendSmsResponse resp = null;
        try{

            Credential cred = new Credential(SecretId, SecretKey);

            HttpProfile httpProfile = new HttpProfile();
            httpProfile.setEndpoint("sms.tencentcloudapi.com");

            ClientProfile clientProfile = new ClientProfile();
            clientProfile.setHttpProfile(httpProfile);

            SmsClient client = new SmsClient(cred, "", clientProfile);

            SendSmsRequest req = new SendSmsRequest();
            String[] phoneNumberSet = mobile;
            req.setPhoneNumberSet(phoneNumberSet);

            req.setTemplateID(TemplateID);
            req.setSign(Sign);

            String[] templateParamSet = templateParam;
            req.setTemplateParamSet(templateParamSet);

            req.setSessionContext(sessionContext);
            req.setSmsSdkAppid(AppID);

            resp = client.SendSms(req);

            return resp;

        } catch (TencentCloudSDKException e) {
            return resp;
        }


    }
}
