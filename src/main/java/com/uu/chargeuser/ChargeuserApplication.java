package com.uu.chargeuser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChargeuserApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChargeuserApplication.class, args);
    }

}
