package com.uu.chargeuser.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity
@Table(name = "permission")
@JsonIgnoreProperties({"handler","hibernateLazyInitializer"})
public class Permission {

    @Id
    @Basic
    @Column(name = "permission_id")
    private Integer permissionId;

    @Basic
    @Column(name = "permission_url")
    private String permissionUrl;

    @Basic
    @Column(name = "permission_name")
    private String permissionName;

    @Basic
    @Column(name = "type")
    private int type;           //等级

    @Basic
    @Column(name = "parent_id")
    private int parentId;       //权限父级

    @Basic
    @Column(name = "sort")      //分类
    private int sort;

    @Override
    public String toString() {
        return "Permission{" +
                "permissionId=" + permissionId +
                ", permissionUrl='" + permissionUrl + '\'' +
                ", permissionName='" + permissionName + '\'' +
                ", type=" + type +
                ", parentId=" + parentId +
                ", sort=" + sort +
                '}';
    }

    public Integer getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(Integer permissionId) {
        this.permissionId = permissionId;
    }

    public String getPermissionUrl() {
        return permissionUrl;
    }

    public void setPermissionUrl(String permissionUrl) {
        this.permissionUrl = permissionUrl;
    }

    public String getPermissionName() {
        return permissionName;
    }

    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }
}
