package com.uu.chargeuser.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * (Member)实体类
 *
 * @author makejava
 * @since 2021-03-06 13:26:31
 */

@Entity
@Table(name = "member")
@JsonIgnoreProperties({"handler", "hibernateLazyInitializer"})
public class Member implements Serializable {

    private static final long serialVersionUID = -37876499531156698L;

    @Id
    /*@GeneratedValue(strategy = GenerationType.IDENTITY)*/
    @Column(name = "sn")
    private String sn;

    @Basic
    @Column(name = "password")
    private String password;

    @Basic
    @Column(name = "platform_sn")
    private String platformSn;

    @Basic
    @Column(name = "type")
    private Integer type;

    @Basic
    @Column(name = "state")
    private Integer state;

    @Basic
    @Column(name = "role")
    private Integer role;

    @Basic
    @Column(name = "operator_member_sn")
    private String operatorMemberSn;

    @Basic
    @Column(name = "project_sn")
    private Integer projectSn;

    @Basic
    @Column(name = "mobile")
    private String mobile;

    @Basic
    @Column(name = "balance")
    private Double balance;

    @Basic
    @Column(name = "actual_balance")
    private Double actualBalance;

    @Basic
    @Column(name = "discount_balance")
    private Double discountBalance;

    @Basic
    @Column(name = "avatar")
    private String avatar;

    @Basic
    @Column(name = "wechat_openid")
    private String wechatOpenid;

    @Basic
    @Column(name = "alipay_userid")
    private String alipayUserid;

    @Basic
    @Column(name = "subscribe")
    private Integer subscribe;

    @Basic
    @Column(name = "nickname")
    private String nickname;

    @Basic
    @Column(name = "country")
    private String country;

    @Basic
    @Column(name = "province")
    private String province;

    @Basic
    @Column(name = "city")
    private String city;

    @Basic
    @Column(name = "sex")
    private Integer sex;

    @Basic
    @Column(name = "charge_count")
    private Integer chargeCount;

    @Basic
    @Column(name = "one_charge_minuts")
    private Integer oneChargeMinuts;

    @Basic
    @Column(name = "cancel_noload_check")
    private Integer cancelNoloadCheck;

    @Basic
    @Column(name = "first_charge_time")
    private Long firstChargeTime;

    @Basic
    @Column(name = "split_account_mode")
    private Integer splitAccountMode;

    @Basic
    @Column(name = "last_login_ip")
    private String lastLoginIp;

    @Basic
    @Column(name = "last_login_time")
    private Long lastLoginTime;

    @Basic
    @Column(name = "create_time")
    private Long createTime;

    @Basic
    @Column(name = "update_time")
    private Long updateTime;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "member_role",
            joinColumns = {@JoinColumn(name = "SN", referencedColumnName = "sn")},
            inverseJoinColumns = {@JoinColumn(name = "ROLE_ID", referencedColumnName = "roleId")})
    private Set<Role> roles;

    @Override
    public String toString() {
        return "Member{" +
                "sn='" + sn + '\'' +
                ", password='" + password + '\'' +
                ", platformSn='" + platformSn + '\'' +
                ", type=" + type +
                ", state=" + state +
                ", role=" + role +
                ", operatorMemberSn='" + operatorMemberSn + '\'' +
                ", projectSn=" + projectSn +
                ", mobile='" + mobile + '\'' +
                ", balance=" + balance +
                ", actualBalance=" + actualBalance +
                ", discountBalance=" + discountBalance +
                ", avatar='" + avatar + '\'' +
                ", wechatOpenid='" + wechatOpenid + '\'' +
                ", alipayUserid='" + alipayUserid + '\'' +
                ", subscribe=" + subscribe +
                ", nickname='" + nickname + '\'' +
                ", country='" + country + '\'' +
                ", province='" + province + '\'' +
                ", city='" + city + '\'' +
                ", sex=" + sex +
                ", chargeCount=" + chargeCount +
                ", oneChargeMinuts=" + oneChargeMinuts +
                ", cancelNoloadCheck=" + cancelNoloadCheck +
                ", firstChargeTime=" + firstChargeTime +
                ", splitAccountMode=" + splitAccountMode +
                ", lastLoginIp='" + lastLoginIp + '\'' +
                ", lastLoginTime=" + lastLoginTime +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", roles=" + roles +
                '}';
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPlatformSn() {
        return platformSn;
    }

    public void setPlatformSn(String platformSn) {
        this.platformSn = platformSn;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public String getOperatorMemberSn() {
        return operatorMemberSn;
    }

    public void setOperatorMemberSn(String operatorMemberSn) {
        this.operatorMemberSn = operatorMemberSn;
    }

    public Integer getProjectSn() {
        return projectSn;
    }

    public void setProjectSn(Integer projectSn) {
        this.projectSn = projectSn;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Double getActualBalance() {
        return actualBalance;
    }

    public void setActualBalance(Double actualBalance) {
        this.actualBalance = actualBalance;
    }

    public Double getDiscountBalance() {
        return discountBalance;
    }

    public void setDiscountBalance(Double discountBalance) {
        this.discountBalance = discountBalance;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getWechatOpenid() {
        return wechatOpenid;
    }

    public void setWechatOpenid(String wechatOpenid) {
        this.wechatOpenid = wechatOpenid;
    }

    public String getAlipayUserid() {
        return alipayUserid;
    }

    public void setAlipayUserid(String alipayUserid) {
        this.alipayUserid = alipayUserid;
    }

    public Integer getSubscribe() {
        return subscribe;
    }

    public void setSubscribe(Integer subscribe) {
        this.subscribe = subscribe;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public Integer getChargeCount() {
        return chargeCount;
    }

    public void setChargeCount(Integer chargeCount) {
        this.chargeCount = chargeCount;
    }

    public Integer getOneChargeMinuts() {
        return oneChargeMinuts;
    }

    public void setOneChargeMinuts(Integer oneChargeMinuts) {
        this.oneChargeMinuts = oneChargeMinuts;
    }

    public Integer getCancelNoloadCheck() {
        return cancelNoloadCheck;
    }

    public void setCancelNoloadCheck(Integer cancelNoloadCheck) {
        this.cancelNoloadCheck = cancelNoloadCheck;
    }

    public Long getFirstChargeTime() {
        return firstChargeTime;
    }

    public void setFirstChargeTime(Long firstChargeTime) {
        this.firstChargeTime = firstChargeTime;
    }

    public Integer getSplitAccountMode() {
        return splitAccountMode;
    }

    public void setSplitAccountMode(Integer splitAccountMode) {
        this.splitAccountMode = splitAccountMode;
    }

    public String getLastLoginIp() {
        return lastLoginIp;
    }

    public void setLastLoginIp(String lastLoginIp) {
        this.lastLoginIp = lastLoginIp;
    }

    public Long getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Long lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }
}
