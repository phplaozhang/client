package com.uu.chargeuser.entity.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
public class MemberDTO {
    private String sn;                      //编号(原optno，cardno)
    private String password;				//密码
    //private int role;						//会员身份 1会员，10运营商，20平台管理员

    public MemberDTO(String sn, String password) {
        this.sn = sn;
        this.password = password;
        //this.role = role;
    }

    @Override
    public String toString() {
        return "MemberDTO{" +
                "sn='" + sn + '\'' +
                ", password='" + password + '\'' +
               // ", role=" + role +
                '}';
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
