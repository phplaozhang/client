package com.uu.chargeuser.dao;

import com.uu.chargeuser.entity.Member;

import java.util.List;

import com.uu.chargeuser.entity.dto.MemberDTO;
import io.lettuce.core.dynamic.annotation.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * (Member)表数据库访问层
 *
 * @author zry
 * @since 2021-03-06 13:26:31
 */
public interface MemberDao extends JpaRepository<Member, Integer> {
    Member findBySn(String sn);
    //@Query(value = "select new com.uu.chargeuser.entity.dto.MemberDTO(it.sn,it.password,it.role) from Member it  where it.sn= :sn")
    //@Query(value = "SELECT it.`id`, it.`item_name`,si.`item_number` FROM item it LEFT JOIN sale_item si ON (it.`id` = si.item_id) WHERE si.`sale_id`=?1",nativeQuery = true) 如果使用本地sql的话，就会报无法转化属性的错误
    @Query(value = "select new com.uu.chargeuser.entity.dto.MemberDTO(it.sn,it.password) from Member it  where it.sn= :sn")
    MemberDTO findMemberDTOBySn(@Param("sn") String sn);
}

